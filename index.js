console.log("Activity Session18")

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/
// code here:

    function sumTwoNumbers(numberOne, numberTwo){
       
    	sum = numberOne + numberTwo;

        console.log(`Displayed sum of 5 and 15: \n ${sum}`);
    }
    sumTwoNumbers(5,15);

    function subtractTwoNumbers(numberThree, numberFour){
       
        subtract = numberThree - numberFour;

        console.log(`Displayed the difference of 20 and 5: \n ${subtract}`);
    }
    subtractTwoNumbers(20, 5);

/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/
// code here:

    function printProductOfTwoNumbers(num1, num2){
        return num1 * num2
    }
    let product = printProductOfTwoNumbers(50, 10);
    console.log(`The product of 50 and 10 is: \n ${product}`);



    function printDivisionOfTwoNumbers(quotient1, quotient2){
        return quotient1 / quotient2
    }
    let finalQuotient = printDivisionOfTwoNumbers(50, 10);
    console.log(`The quotient of 50 and 10: \n ${finalQuotient}`);


    


/*
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/
// code here:


/**
 * Calcuates radius area of the given input
 * @param {*} radius1 Input to be calculated
 * @returns calculated radius area
 */
    function calculateRadiusArea(radius1){
		console.log("The result of getting the area of a circle with 15 radius:")
		exponent = 3.14 * radius1 ** 2
        return exponent
    }

    let circleArea = calculateRadiusArea(15)
	console.log(circleArea)
   
    
/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/
// code here:

    function totalAverage(subject1, subject2, subject3, subject4){
		console.log('The average of 20, 40, 60 and 80');
        return (subject1 + subject2 + subject3 + subject4) / 4;
       
    }
        let averageVar = totalAverage (20, 40, 60, 80);
        console.log(averageVar);
        

/*
    5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score (38) and the total score (50).
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than (75) the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
// code here:
	


    function printMyPassingScore (score, totalScore){

		percentageResult = (score / totalScore) * 100;
		console.log('Is 38/50 a passing score?:');
		let isPassed = percentageResult >= 75
        return isPassed;
    }
    let isPassingScore = printMyPassingScore (38, 50);
    console.log(isPassingScore);
